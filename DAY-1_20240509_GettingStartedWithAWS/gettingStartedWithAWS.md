#### Session Video:

```
https://drive.google.com/file/d/1Z-K4DjfDaxFvO0hBxfv1URikPDFp6tni/view?usp=sharing
```

#### Getting Started with AWS 

```
1. Create Account with AWS 
    1.1 Login to AWS as Root user
    1.2 AWS Console 
    1.3 Header Menu
    1.4 Global Infrastructure of AWS : 33 - 105 | 6 - 18
    1.5 By Default 17 regions are enabled 
    1.6 Remaining 11 regions you can enable yourselves 
    1.7 For 5 more regions you should create ticket with AWS 
    1.8 Understanding the AWS Services : 1. Global vs 2. Region 

2. Create Account with GitLab :
    - From my end I have created repository to share Running Notes, & Session Video 

```

#### Task-1: Explore unto AWS and try to understand Global vs Region Services 